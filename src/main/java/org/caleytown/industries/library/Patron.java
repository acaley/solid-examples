package org.caleytown.industries.library;

public class Patron {
   private String name;
   public Patron(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
