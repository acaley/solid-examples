package org.caleytown.industries.library;

public class Book {
    private String title;
    private String isbn;
    public Book(String isbn, String title) {
        this.title = title;
        this.isbn = isbn;
    }

    public void checkoutTo(Patron patron) {
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }
}
