package org.caleytown.industries.library;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

import java.util.Optional;

public class Library {
    private Session dbSession;
    public Library(Session dbSession) {
        this.dbSession = dbSession;
    }

    public void loginAndCheckoutBookToPatron(String user, String password, String isbn, String patronName) throws ItemNotFoundException, UserUnauthorizedException {
        if(!isUserAuthorizedForCheckout(user, password)) {
            throw new UserUnauthorizedException("User:" + user + " is not allowed to checkout");
        }
        Optional<Book> book = getBookFromIsbn(isbn);
        if(book.isEmpty()) {
            throw new ItemNotFoundException("Could not find book with isbn:" + isbn);
        }
        Optional<Patron> patron = getPatronFromName(patronName);
        if(patron.isEmpty()) {
            throw new ItemNotFoundException("Could not find patron with name:" + patronName);
        }
        book.get().checkoutTo(patron.get());
    }

    private boolean isUserAuthorizedForCheckout(String user, String password) {
        return true;
    }

    private Optional<Patron> getPatronFromName(String patronName) {
        PreparedStatement patronByName = dbSession.prepare("SELECT * FROM PATRONS WHERE name=?");
        ResultSet rs = dbSession.execute(patronByName.bind(patronName));
        if(rs.all().size() == 0) {
            return Optional.empty();
        }
        return Optional.of(new Patron(rs.one().getString("name")));
    }

    private Optional<Book> getBookFromIsbn(String isbn) {
        PreparedStatement bookByIsbn = dbSession.prepare("SELECT * FROM BOOKS WHERE isbn=?");
        ResultSet rs = dbSession.execute(bookByIsbn.bind(isbn));
        if(rs.all().size() == 0) {
            return Optional.empty();
        }
        return Optional.of(new Book(rs.one().getString("isbn"),
                                    rs.one().getString("title")));
    }
}
